/// some common definitions shared by both client and server side
unit interfaces;

interface

uses
{//$I synopse.inc}
  SynCommons,
  SynLog,
  mORMot;

type
  Tprivileges = class(TSQLRecord)
  private
    fuserid: string;
    fpriv: integer;
  published
    property userid: string read fuserid write fuserid;
    property priv: integer read fpriv write fpriv;
  end;

  ICalculator = interface(IInvokable)
    ['{9A60C8ED-CEB2-4E09-87D4-4A16F496E5FE}']
        function ListPeople(searchstring: RawUTF8): RawUTF8;
  end;



const
  ROOT_NAME = 'root';
  PORT_NAME = '81';
  APPLICATION_NAME = 'RestService';

implementation

initialization

// so that we could use directly ICalculator instead of TypeInfo(ICalculator)
TInterfaceFactory.RegisterInterfaces([TypeInfo(ICalculator)]);

end.
