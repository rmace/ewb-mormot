unit ewbmormot;

// v 0.0.3 Sept 2016 Erick Engelke - added mormotautoadd flag
// v 0.0.2 July 2016 Erick Engelke - added AD authentication
// v 0.0.1 Jan 2016 Erick Engelke - started
interface

uses
{//$I synopse.inc}
  windows,
  // ansistrings,
  types, SysUtils, inifiles,
  // for mormot
  SynCommons,
  SynLog,
  mORMot, mormotsqlite3,
  // libsodium for negotiation
  LibSodium; // in 'LibSodium.pas',

type
  INetAuth = interface(IInvokable)
    ['{3EFE5AD7-07AC-4207-9061-EC51EECAFD51}']
    // GetPublicKey - get server's public key
    function GetPublicKey: RawUTF8;
    // SumbitPass - userid, password encrypted with server pub key, client's pvt key
    function SubmitPass(userid, password: string; mypublickey: string): boolean;
    // SubmitOAuth2 - key
  end;

  // ---------------------------------------------------------
  TServiceAuth = class(TInterfacedObject, INetAuth)
  public
    function GetPublicKey: RawUTF8;
    // SumbitPass - userid, password encrypted with server pub key, client's pvt key
    function SubmitPass(userid, password: string; mypublickey: string): boolean;
    // SubmitOAuth2 - key
  end;

  TEWBMormot = class
  private
  public
    grpuser, grp: TSQLAuthGroup;
    server_public_key: string; // usable
    myaServer: TSQLRest;
    useraccessrights: RawUTF8;
    guestaccessrights: RawUTF8;
    mormotautoadduserids: boolean; // defaults to false
    // automatically add authenticated users to userspace
    grpuserid: TID; // user group for autocreated accounts defaults to 0

    constructor Create;

    function GetGroup(userid: RawUTF8): integer;
    function DoAddUser(aServer: TSQLRest;
      userid, password, name: string): boolean;
    procedure LimitAccess(group: RawUTF8; code: RawUTF8 = '0,0,0,0,0');

    function unseal(s: string): string;
    procedure LibSodiumKeySetup;
    function getlocaluser: RawUTF8;
    function UTF8ToDate(s: RawUTF8): TDate; // 2008-4-4
    function TestADSUseridPassword(domain, userid, password: string;
      var name: string): boolean;
    procedure CreateEWBMormotSpecialAccounts(aServer: TSQLRest);
  end;

var
  EWBMormotUtils: TEWBMormot;

implementation

// ============================================================================

var
  sodium_inited: boolean = false;
  server_secretkey: array [0 .. ls_crypto_box_PUBLICKEYBYTES - 1] of byte;
  server_publickey: array [0 .. ls_crypto_box_SECRETKEYBYTES - 1] of byte;

  addomain: string; // store active directory domain
  dateformatsettings: TFormatSettings; // used for date conversion
  dateformatinited: boolean = false;

  // ---------------------------------------------------------

  // ------ password stuff

constructor TEWBMormot.Create;
begin
  inherited;
  guestaccessrights := '2,1-2,0,1-2,0,1-2,0,1-2,0';
  useraccessrights := '2,0,0,0,0';
end;

procedure StripCNStuff(var s: string);
var
  i: integer;
begin
  i := Pos('=', s);
  if i > 0 then
    Delete(s, 1, i);
  i := Pos(',', s);
  if i > 0 then
    Delete(s, i, Length(s));
end;

// ---------------------------------------------------------

function LogonUserW(lpszUsername: PChar; lpszDomain: PChar; lpszPassword: PChar;
  dwLogonType: DWORD; dwLogonProvider: DWORD; var phToken: THANDLE): boolean;
  stdcall; external 'advapi32';

function LocalTestADSUseridPassword(domain, userid, password: string;
  var name: string): boolean;
const
  LOGON32_LOGON_BATCH = 4;
  LOGON32_LOGON_NETWORK = 3;
var
  token: THANDLE;
begin
  result := false;
  if (Pos('@', userid) = 0) and (Pos('\', userid) = 0) then

    if LogonUserW(PChar(userid), PChar(domain), PChar(password),
      LOGON32_LOGON_NETWORK, 0, token) then
    begin
      CloseHandle(token);
      result := True;
    end;
end;

// ---------------------------------------------------------

// ---------------------------------------------------------

procedure TEWBMormot.LimitAccess(group: RawUTF8; code: RawUTF8 = '0,0,0,0,0');
var
  grpz: TSQLAuthGroup;
begin
  grpz := TSQLAuthGroup.Create(myaServer, 'Ident = ?', [group]);
  if grpz.ID > 0 then
  begin
    grpz.AccessRights := code;
    myaServer.Update(grpz);
  end;
  grpz.free;
end;
// ---------------------------------------------------------

function TEWBMormot.TestADSUseridPassword(domain, userid, password: string;
  var name: string): boolean;
begin
  result := LocalTestADSUseridPassword(domain, userid, password, name);
end;

// -----------------------------------------------------------------------------

function TEWBMormot.UTF8ToDate(s: RawUTF8): TDate; // 2008-4-4
begin
  if not dateformatinited then
  begin
{$IFDEF DELPHI}
    dateformatsettings := TFormatSettings.Create;
    dateformatsettings.ShortDateFormat := 'yyyy-M-d';
    dateformatsettings.DateSeparator := '-';
    dateformatinited := True;
{$ENDIF}
  end;
  result := StrToDate(s, dateformatsettings);
end;

// ---------------------------------------------------------

function TEWBMormot.getlocaluser: RawUTF8;
var
  context: PServiceRunningContext;
begin
  context := @ServiceContext;
  result := context^.Request.SessionUserName;
end;
// ---------------------------------------------------------

function testencryption: boolean;
var
  msg, msg2: AnsiString;
  myciphertext: AnsiString;
  c: integer;
begin
  result := True;
  exit;

  msg := 'hi there erick';
  myciphertext := 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf';
  myciphertext := myciphertext + myciphertext;
  myciphertext := myciphertext + myciphertext;

  //SetLength(myciphertext, 4 * ls_crypto_box_PUBLICKEYBYTES + Length(msg) + 1);
//   fillchar( myciphertext, Length( myciphertext ), 0 );

  c := crypto_box_seal(pansichar(myciphertext), pansichar(msg), Length(msg),
    @server_publickey);
  c := length(myciphertext);
//  SetLength(myciphertext, c);
  msg2 := '';
  SetLength(msg2, Length(myciphertext) * 10);
  result := crypto_box_seal_open(pansichar(msg2), pansichar(myciphertext),
    Length(myciphertext), @server_publickey, @server_secretkey) = 0;

end;
// ---------------------------------------------------------

procedure TEWBMormot.LibSodiumKeySetup;
var
  hexBuf: AnsiString;
  server_private_key: String;
  aprivatekey, apublickey: AnsiString;
  c: integer;
  len: dwSize_T;
  path: string;
begin

  if not sodium_dllLoaded then
  begin
    raise Exception.Create('Fatal Error: could not load "' + sodium_dllFileName
      + '"! Missing a Dependency?');
    // writeln('ie. MSVC v100 runtime requires mfc100.dll, msvcp100.dll, and msvcr100.dll');
    halt;
  end;

  sodium_init;

  path := ExtractFilePath(paramstr(0));
  path := path + 'mormot.ini';
  with TIniFile.Create(path) do
  begin
    server_public_key := ReadString('server', 'publickey', '');
    server_private_key := ReadString('server', 'privatekey', '');
    addomain := ReadString('server', 'domain', '');
    if addomain = '' then
    begin
      raise Exception.Create('Could not load domain for password checks');
      halt;
    end;

    if (server_public_key = '') or (server_private_key = '') then
    begin
      // Create server's secret and public keys
      randombytes(@server_secretkey, sizeof(server_secretkey));

      crypto_box_keypair(@server_publickey, @server_secretkey);

      if not testencryption then
      begin
        raise Exception.Create('ERROR: Decrypting my message');
        halt;
      end;

      SetLength(hexBuf, ls_crypto_box_PUBLICKEYBYTES * 2 + 1);
      sodium_bin2hex(@hexBuf[1], Length(hexBuf), @server_publickey,
        ls_crypto_box_PUBLICKEYBYTES);

      server_public_key := hexBuf;

      SetLength(hexBuf, ls_crypto_box_PUBLICKEYBYTES * 2 + 1);
      sodium_bin2hex(@hexBuf[1], Length(hexBuf), @server_secretkey,
        ls_crypto_box_PUBLICKEYBYTES);

      server_private_key := hexBuf;

      WriteString('server', 'publickey', server_public_key);
      WriteString('server', 'privatekey', server_private_key);
      UpdateFile;
      free;
    end
    else
    begin
      // load ansi versus multibyte
      aprivatekey := server_private_key;
      apublickey := server_public_key;
      len := Length(apublickey);
      sodium_hex2bin(pansichar(@server_publickey[1]), sizeof(server_publickey),
        pansichar(@apublickey[1]), Length(apublickey), nil, len, nil);

      len := Length(aprivatekey);
      sodium_hex2bin(pansichar(@server_secretkey[1]), sizeof(server_secretkey),
        pansichar(@aprivatekey[1]), Length(aprivatekey), nil, len, nil);

      if not testencryption then
      begin
        raise Exception.Create('ERROR: Decrypting my message');
        halt;
      end;

    end;

    free;

  end;
end;
// ---------------------------------------------------------

function TEWBMormot.unseal(s: string): string;
var
  clear: AnsiString;
  bin: AnsiString;
  hex: AnsiString;
  len: DWORD;
begin
  //
  result := '';
  if s = '' then
    raise Exception.Create('invalid encrypted string');

  SetLength(clear, Length(s) * 4);
  len := Length(clear);
  while len > 0 do
  begin
    clear[len] := #0;
    dec(len);
  end;

  SetLength(bin, Length(clear));

  hex := s;

  len := 0;

  sodium_hex2bin(pansichar(@bin[1]), Length(bin), pansichar(@hex[1]),
    Length(hex), nil, len, nil);

  SetLength(bin, len);

  if 0 <> crypto_box_seal_open(pansichar(clear), pansichar(bin), Length(bin),
    @server_publickey, @server_secretkey) then
  begin
    raise Exception.Create('Error decrypting');
  end;
  SetLength(clear, {system.ansistrings.}strlen(pansichar(clear)));
  result := clear;
end;

// ---------------------------------------------------------

function TServiceAuth.GetPublicKey: RawUTF8;
begin

  // Tcrypto_box_curve25519xsalsa20poly1305_seed_keypair(serverprivatekey,
  // serversharedkey, 'a23423423423436346436');

  result := EWBMormotUtils.server_public_key;
end;
// ---------------------------------------------------------

function TEWBMormot.GetGroup(userid: RawUTF8): integer;
var
  user: TSQLAuthUser;
begin
  try
    result := -1;
    user := TSQLAuthUser.Create(myaServer, 'LogonName = ?', [userid]);
    if user.ID = 0 then
    begin
      raise Exception.Create('ERROR: ' + userid + ' not found ');
    end
    else
    begin
      result := integer(user.GroupRights);
      // raise Exception.Create('Found user ' + userid + ' : group : ' + IntToStr(result));
    end;
  finally
    user.free;
  end;
end;

// --------------------------------------------------------
function TEWBMormot.DoAddUser(aServer: TSQLRest;
  userid, password, name: string): boolean;
var
  user: TSQLAuthUser;
begin
  user := TSQLAuthUser.Create(aServer, 'LogonName = ?', [userid]);
  try
    // group id = 0 if new, or non-0 if existing
    user.LogonName := userid;
    user.DisplayName := StringToUTF8(name);
    user.PasswordPlain := password;

    user.GroupRights := TSQLAuthGroup(pointer(grpuserid));

    // was ID 0, meaning not existing, otherwise use update
    result := aServer.AddOrUpdate(user) > 0;
  except
    result := false;
  end;
end;

// ---------------------------------------------------------
function DoSetPassword(aServer: TSQLRest; userid, pass: RawUTF8): boolean;
var
  user: TSQLAuthUser;

begin
  EWBMormotUtils.getlocaluser;
  user := TSQLAuthUser.Create(aServer, 'LogonName = ?', [userid]);
  try
    if user.ID = 0 then
    begin
      // (userid, ' not found ');
      result := false;
    end
    else
    begin
      user.PasswordPlain := pass;
      aServer.Update(user);
      // writeln('Password changed');
      result := True;
    end;
  finally
    user.free;
  end;
end;

// ---------------------------------------------------------
function TServiceAuth.SubmitPass(userid, password: string;
  mypublickey: string): boolean;
var
  // hexuserid, hexpassword: AnsiString;
  plainuserid, plainpassword: AnsiString;
  binuserid, binpassword: AnsiString;
  len: DWORD;
  name: string;
  name8: RawUTF8;
  tempgroup: integer;
begin
  //
  try
    plainuserid := EWBMormotUtils.unseal(userid);
    plainpassword := EWBMormotUtils.unseal(password);

    // password test
    if EWBMormotUtils.TestADSUseridPassword(addomain, plainuserid,
      plainpassword, name) then
    // if (plainuserid = 'erick') and (plainpassword = 'asdf') then
    begin
      // see if it is general user
      try
        tempgroup := EWBMormotUtils.GetGroup(StringToUTF8(plainuserid));

      except
        on E: Exception do
          // does not exist, add userid if so inclined
          if EWBMormotUtils.mormotautoadduserids then
          begin
            //
            EWBMormotUtils.DoAddUser(EWBMormotUtils.myaServer, plainuserid,
              plainpassword, name);
            tempgroup := EWBMormotUtils.GetGroup(StringToUTF8(plainuserid));
          end
          else
            raise;
      end;
      if tempgroup <> EWBMormotUtils.grpuser.ID then
        raise Exception.Create('Error: invalid group for password change (acct:'
          + plainuserid + ':' + IntToStr(tempgroup) + ')');
      // save the userid/password
      if not DoSetPassword(EWBMormotUtils.myaServer, StringToUTF8(plainuserid),
        StringToUTF8(plainpassword)) then
        raise Exception.Create('userid not present in DB');

      result := True;
    end;
  except
    on E: Exception do
      result := false;
  end;
end;

procedure SaveDebug(s: string);
begin

end;

// ---------------------------------------------------------
procedure TEWBMormot.CreateEWBMormotSpecialAccounts(aServer: TSQLRest);

var
  grpid: TID;
  usr: TSQLAuthUser;

  rights: TSQLAccessRights;
  i: integer;
begin

  myaServer := aServer;
  grpuser := TSQLAuthGroup.Create(aServer, 'Ident = ?', ['User']);
  if grpuser.ID = 0 then
  begin
    grpuser.Ident := 'User';
    grpuser.SessionTimeout := 60;
    grpuser.AccessRights := useraccessrights;
    // '14,3-256,0,3-256,0,3-256,0,3-256,0';
    grpuserid := aServer.AddOrUpdate(grp, True);
  end
  else
    grpuserid := grpuser.ID;

  // search for needed group:
  grp := TSQLAuthGroup.Create(aServer, 'Ident = ?', ['EWBlogin']);
  // "RowID":5,"Ident":"EWBlogin","SessionTimeout":60,"AccessRights":"14,3-256,0,3-256,0,3-256,0,3-256,0"}]}

  if grp.ID = 0 then
  begin
    grp.Ident := 'EWBlogin';
    grp.SessionTimeout := 60;

    writeln('GOT ACCESS RIGHTS ', grp.AccessRights);
    grpid := aServer.AddOrUpdate(grp, True);
  end
  else
    grpid := grp.ID;

  grp.AccessRights := guestaccessrights; // '2,0,0,0,0';
  TSQLLog.Add.Log(sllTrace, 'EWBLogin accessrights % ',
    [UTF8toString(grp.AccessRights)]);
  aServer.Update(grp);
  grp.free;

  usr := TSQLAuthUser.Create(aServer, 'LogonName = ?', ['EWBlogin']);
  if usr.ID = 0 then
  begin
    usr.LogonName := 'EWBlogin';
    usr.DisplayName := 'EWBlogin';
    usr.PasswordPlain := 'synopse';
    usr.GroupRights := TSQLAuthGroup(pointer(grpid));
    aServer.AddOrUpdate(usr, True);
  end;
  usr.free;

end;
// ---------------------------------------------------------

initialization

TInterfaceFactory.RegisterInterfaces([TypeInfo(INetAuth)]);

end.
