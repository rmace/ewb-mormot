program enterprise;

{$mode delphi}{z$H+}


uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Classes,
  //Windows,
  SysUtils,
  SynCommons,
  SynLog,
  mORMot,
  mORMotHttpServer,
  SynZip,
  SynCrtSock,
  SynDb,
  SynDBDataSet,
  //  SynDBZeos,
  mORMotSQLite3,
  mormotdb,
  SynSQLite3Static,
  interfaces in 'interfaces.pas',
  ewbmormot in 'ewbmormot.pas',
  LibSodium in 'LibSodium.pas';
//  phdthesis in 'phdthesis.pas',
//  base64unit in 'base64unit.pas',
//  webserver in 'webserver.pas';

var
  aModel: TSQLModel;
  aHTTPServer: TSQLHttpServer;
  sfs: TServiceFactoryServer;

  // mysql server
  AServerName: RawUTF8;
  PropsClass: TSQLDBConnectionPropertiesClass;
  props: TSQLDBConnectionProperties;
  AServer: TSQLRestServer; //ServerDB;
  AClient: TSQLRestCLientDB;

type
  // ---------------------------------------------------------

  TServiceCalculator = class(TInterfacedObject, ICalculator)
  public
    function listpeople(searchstring: RawUTF8): RawUTF8;

  end;


  // ---------------------------------------------------------
  function TServiceCalculator.ListPeople(searchstring: RawUTF8): RawUTF8;
  var
    d: TSQLAuthUser;
    Count: integer;
  begin
    Result := '';
    // let's get the userid of the current context
    writeln('current user : ' + ewbmormotutils.getlocaluser);

    d := TSQLAuthUser.CreateAndFillPrepare(aserver, 'LogonName like ?', [searchstring]);
    if assigned(d) then
    begin
      try
        Count := 0;
        while d.FillOne do
        begin
          if Count > 0 then
            Result := Result + ',';
          Inc(Count);
          Result := Result + ObjectToJSON(d);

        end;
      finally
       d.Free;
      end;
    end;

    Result := '[' + Result + ']';
  end;

  // searchpeople(


  // ---------------------------------------------------------


  // ---------------------------------------------------------
  // ---------------------------------------------------------


  // ---------------------------------------------------------

  // ---------------------------------------------------------

  // ---------------------------------------------------------

  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // Handle privilege creation
  procedure CreateLocalSpecialAccounts(AServer: TSQLRest);
  begin
    ewbmormotutils.DoAddUser(AServer, 'erick', 'happy', '');
  end;




  // ---------------------------------------------------------

const
  dbserver: RawUTF8 = 'ecserv19.uwaterloo.ca:3306';
  dbname: RawUTF8 = 'phoenix';
  dbuser: RawUTF8 = 'phoenixadmin';
  //  dbpass: RawUTF8 = 'MqFASDFASDF';

  // var
  // v: variant;

begin
  randomize;
  // define the log level
  with TSQLLog.Family do
  begin
    Level := LOG_VERBOSE;
    EchoToConsole := LOG_VERBOSE; // log all events to the console
  end;

  EWBMormotUtils := TEWBMormot.Create;
  EWBMormotUtils.LibSodiumKeySetup;

  // create a Data Model
  aModel := TSQLModel.Create([TSQLAuthGroup, TSQLAuthUser, TPrivileges], ROOT_NAME);
  try
    //initialize a TObjectList-based database engine
    aServer := TSQLRestServerFullMemory.Create(aModel, 'test.json', False, True);
    AClient := TSQLRestClientDB.Create(aModel, nil, string('file.db3'),
      TSQLRestServerDB, False, '');
 {$IFDEF NEVER}
{    // start of props
    PropsClass := TSQLDBZEOSConnectionProperties;
    AServerName := TSQLDBZEOSConnectionProperties.URI(dMySQL, dbserver);
    props := PropsClass.Create(AServerName, dbname, dbuser, dbpass );
    // enda props


    VirtualTableExternalRegister(aModel, TSQLAuthGroup, props,'SQLAuthGroup');
    VirtualTableExternalRegister(aModel, TSQLAuthUser, props,'SQLAuthUser');
    VirtualTableExternalRegister(aModel, TPrivileges, props,'Privileges');
    VirtualTableExternalRegister(aModel, TPhDGSStaff, props,'PhDGSStaff');
    VirtualTableExternalRegister(aModel, TPhDDepartment, props,'PhDDepartment');
    VirtualTableExternalRegister(aModel, TPhDStudent, props,'PhDStudent');
    VirtualTableExternalRegister(aModel, TPhDPerson, props,'PhDPerson');
    VirtualTableExternalRegister(aModel, TLocalFile, props,'PhDLocalFile');
    //VirtualTableExternalRegisterAll( aModel, props, False );
        // launch the HTTP server
}
{$endif}

    AClient := TSQLRestClientDB.Create(aModel, nil, 'temp.db3',
      TSQLRestServerDB, True, '');
    AServer := TSQLRestServerDB.Create(aModel,
      ChangeFileExt(ExeVersion.ProgramFileName, '.db3'), True, '');
    aHTTPServer := TSQLHttpServer.Create(PORT_NAME, [AServer], '+',
      useHttpApiRegisteringURI);

    TSQLRestServerDB(AServer).CreateMissingTables;
    EWBMormotUtils.CreateEWBMormotSpecialAccounts(AServer);
    //    EWBMormotUtils.CreateEWBMormotSpecialAccounts(AClient);
    CreateLocalSpecialAccounts(AServer);
{
    with EWBMormotUtils do
    begin
      LimitAccess('Administrator');
      LimitAccess('Guest');
      LimitAccess('User', '2,1-2,1-2,1-2,1-2');
      LimitAccess('Admin');
      LimitAccess('Supervisor');
    end;
}
    // initialize PhD subsystem
    //   PhDInit(EWBMormotUtils, AServer);

    try
      // register our ICalculator service on the server side
      AServer.ServiceDefine(TServiceCalculator, [ICalculator], sicShared);
      //      AServer.ServiceDefine(TServicePhDThesis, [IPhDThesis], sicShared);

      //      AServer.ServiceDefine(TServiceAuth, [INetAuth], sicShared);
      // default: guests not usually allowed to execute services, so we change that
      //      sfs := AServer.Services['NetAuth'] as TServiceFactoryServer;
      //      sfs.AllowAll;
      sfs := AServer.Services['Calculator'] as TServiceFactoryServer;
      sfs.DenyAll;
      sfs.AllowAllByName(StringToUTF8('User')); // ID([3]);

      // setting this flag causes new accounts to be autocreated
      // when people log in with a valid AD account
      EWBMormotUtils.mormotautoadduserids := True;

      // old props was here:code

      try
        aHTTPServer.AccessControlAllowOrigin := '*';
        //        InitWebServer;
        // for AJAX requests to work
        writeln(#10'Background server is running.'#10);
        writeln('Press [Enter] to close the server.'#10);
        readln;
      finally
        aHTTPServer.Free;
        //        ShutDownWebServer;
      end;
    finally
      AServer.Free;
      //      AClient.Free;
    end;
  finally
    aModel.Free;
  end;
end.




